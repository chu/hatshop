﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_Manage_ManageProductCategory : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        CategoryModel model = new CategoryModel();
        Category cat = createCategory();

        Label1.Text = model.InsertCategory(cat);

    }

    private Category createCategory()
    {
        Category p = new Category();
        p.CategoryName = TextBox1.Text;

        return p;

    }


}