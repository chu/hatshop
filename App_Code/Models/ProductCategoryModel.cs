﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CategoryCategoryModel
/// </summary>
public class CategoryModel
{
    public string InsertCategory(Category Category)
    {
        try
        {
            HatSaleEntities db = new HatSaleEntities();
            db.Category.Add(Category);
            db.SaveChanges();

            return "HatCategory <" + Category.CategoryName + "> is added to website.";
        }
        catch (Exception e)
        {
            return "There is an ERROR: " + e;
        }

    }

    public string UPdateCategory(int id, Category Category)
    {
        try
        {
            HatSaleEntities db = new HatSaleEntities();
            Category a = db.Category.Find(id);
            a.CategoryId = Category.CategoryId;
            a.CategoryName = Category.CategoryName;


            db.SaveChanges();
            return "HatCategory <" + Category.CategoryName + "> is updated.";


        }
        catch (Exception e)
        {
            return "There is an ERROR: " + e;
        }
    }

    public string DeleteProduct(int id)
    {
        try
        {
            HatSaleEntities db = new HatSaleEntities();

            Product product = db.Products.Find(id);

            db.Products.Attach(product);
            db.Products.Remove(product);
            db.SaveChanges();

            return "HatCategory <" + product.Name + "> has been deleted";


        }
        catch (Exception e)
        {
            return "There is an ERROR: " + e;
        }
    }

}