﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CartModel
/// </summary>
public class CartModel
{
    public string InsertCart(Cart cart)
    {
        try
        {
            HatSaleEntities db = new HatSaleEntities();
            db.Cart.Add(cart);
            db.SaveChanges();

            return cart.ClientId + " is inserted.";
        }
        catch (Exception e)
        {
            return "There is an ERROR: " + e;
        }

    }

    public string UPdateCart(int id, Cart cart)
    {
        try
        {
            HatSaleEntities db = new HatSaleEntities();
            Cart a = db.Cart.Find(id);
            a.OrderId = cart.OrderId;
            a.Id = cart.Id;
            a.ClientId = cart.ClientId;
            a.Name = cart.Name;
            a.Quantity = cart.Quantity;
            a.Price = cart.Price;
            a.Colour = cart.Colour;
            a.Subtotal = cart.Subtotal;
            a.GrandTotal = cart.GrandTotal;
            a.Gst = cart.Gst;


            

            db.SaveChanges();
            return cart.OrderId + " are updated.";


        }
        catch (Exception e)
        {
            return "There is an ERROR: " + e;
        }
    }

    public string DeleteCart(int id)
    {
        try
        {
            HatSaleEntities db = new HatSaleEntities();

            Cart cart = db.Cart.Find(id);

            db.Cart.Attach(cart);
            db.Cart.Remove(cart);
            db.SaveChanges();

            return cart.OrderId + " has been deleted";


        }
        catch (Exception e)
        {
            return "There is an ERROR: " + e;
        }
    }
}