﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ProductModel
/// </summary>
public class ProductModel
{
    public string InsertProduct(Product product)
    {
        try
        {
            HatSaleEntities db = new HatSaleEntities();
            db.Products.Add(product);
            db.SaveChanges();

            return product.Name + "is inserted.";
        }
        catch (Exception e)
        {
            return "There is an ERROR: " + e;
        }

    }

    public string UPdateProduct(int id, Product product)
    {
        try
        {
            HatSaleEntities db = new HatSaleEntities();
            Product a = db.Products.Find(id);
            a.Id = product.Id;
            a.Category = product.Category;
            a.Name = product.Name;
            a.Price = product.Price;
            a.Description = product.Description;
            a.Image = product.Image;
            a.Supplier = product.Supplier;
            a.Colour = product.Colour;

            db.SaveChanges();
            return product.Name + "are updated.";


        }
        catch (Exception e)
        {
            return "There is an ERROR: " + e;
        }
    }

    public string DeleteProduct(int id)
    {
        try
        {
            HatSaleEntities db = new HatSaleEntities();

            Product product = db.Products.Find(id);

            db.Products.Attach(product);
            db.Products.Remove(product);
            db.SaveChanges();

            return product.Name + "has been deleted";
            

        }
        catch (Exception e)
        {
            return "There is an ERROR: " + e;
        }
    }


}